import sys
import time
import shutil
import os
import re

from subprocess import call
from time import gmtime, strftime
from fabric import api
from fabric.api import run
from ploy import Controller


class Upgrader:

    def __init__(
            self,
            services,
            backup_paths,
            backup_files,
            backup_postgre_dbs,
            backup_mariadb_dbs,
            backup_sqlite_dbs,
            backup_has_ldap,
            ansible_vars,
            master_host='jailhost',
            configfile='../etc/ploy.conf',
            hostname=None):
        self.services = services
        self.backup_paths = backup_paths
        self.backup_files = backup_files
        self.backup_postgre_dbs = backup_postgre_dbs
        self.backup_mariadb_dbs = backup_mariadb_dbs
        self.backup_sqlite_dbs = backup_sqlite_dbs
        self.backup_has_ldap = backup_has_ldap
        self.ansible_vars = ansible_vars

        self.master_host = master_host

        self.ctrl = Controller()
        self.ctrl.configfile = configfile

        self.hostname = self.ansible_vars[
            'inventory_hostname_short'] if hostname is None else hostname
        self.hostname_short = re.match('jailhost-(.*)', self.hostname).group(1)

        self.mounts = []
        if 'mounts' in self.ctrl.instances[self.hostname].config:
            mount_map = self.ctrl.instances[self.hostname].config['mounts']
            for mount in mount_map:
                self.mounts.append(mount['dst'])

    def _build_jail_string(self, testname_short, ip):
        test_config = '\n[ez-instance:' + testname_short + ']\n'

        if 'fabfile' in self.ctrl.instances[self.hostname].config:
            test_config += 'fabfile = ' + \
                self.ctrl.instances[self.hostname].config['fabfile'] + '\n'

        test_config += 'ip = ' + ip + '\n'
        test_config += 'roles =\n'

        roles = self.ctrl.instances[self.hostname].config['roles'].strip().split('\n')
        for role in roles:
            test_config += '\t' + role + '\n'

        if 'mounts' in self.ctrl.instances[self.hostname].config:
            test_config += 'mounts =\n'

            mounts = self.ctrl.instances[self.hostname].config['mounts']
            for mount in mounts:
                test_config += '\t' + 'src=' + mount['src'] + ' dst=' + mount['dst'] + ' ro=' + str(mount['ro']) + '\n'

        test_config += '\n'

        return test_config

    def upgrade(self, ip):
        self.ctrl.instances[self.master_host].do('stop_services', self.hostname_short, self.services, True)
        id = self.ctrl.instances[self.master_host].do(
            'backup',
            self.hostname_short,
            self.backup_postgre_dbs,
            self.backup_mariadb_dbs,
            self.backup_has_ldap)

        jailname = self.hostname[:self.hostname.rfind('_')] if re.search(
            r'\d+$', self.hostname) is not None else self.hostname

        testname = jailname + '_' + strftime("%Y%m%d%H%M%S", gmtime())
        testname_short = re.match('jailhost-(.*)', testname).group(1)

        configpath = os.path.realpath('../host_vars/' + self.hostname + '.yml')

        if os.path.exists(configpath):
            test_configpath = os.path.realpath('../host_vars/' + testname + '.yml')
            shutil.copy(configpath, test_configpath)

        test_config = self._build_jail_string(testname_short, ip)
        with open('../etc/ploy.conf', 'a') as f:
            f.write(test_config)

        self.ctrl = Controller()
        self.ctrl.configfile = '../etc/ploy.conf'

        # otherwise a possible postgre connection is going to fail
        self.ctrl.instances[self.hostname].stop()
        self.ctrl.instances[testname].start()

        print('waiting 60 seconds to ensure that the ssh daemon is running...')
        time.sleep(60)

        #self.ctrl.instances[testname].configure() # fails, because an old ctrl object is used
        call(['ploy', '-c', '../etc/ploy.conf', 'configure', testname], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})

        self.ctrl.instances[self.master_host].do('stop_services', testname_short, self.services, True)

        self.ctrl.instances[self.master_host].do(
            'restore',
            self.hostname_short,
            testname_short,
            self.backup_postgre_dbs,
            self.backup_mariadb_dbs,
            self.backup_sqlite_dbs,
            self.backup_has_ldap,
            self.backup_paths,
            self.backup_files,
            self.mounts,
            id)
        self.ctrl.instances[self.master_host].do('start_services', testname_short, self.services, True)
