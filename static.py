from bsdploy import fabutils
from fabric.api import env
from fabric.api import run

ansible_vars = env.instance.get_ansible_variables()

def upload_data(filepath):
    remote_path = '/usr/local/www/static'

    fabutils.rsync(
            '-pthrvz',
            filepath,
            'root@' + ansible_vars['ploy_ip'] + ':' + remote_path)
    run('chown -R www:www ' + remote_path)
