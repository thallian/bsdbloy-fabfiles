from fabric.api import cd, run

def make_admin(username):
    admin_cmd = 'sudo -u mediagoblin ./bin/gmg makeadmin ' + username
    with cd('/usr/local/mediagoblin/mediagoblin'):
        run(admin_cmd, shell_escape=False)
