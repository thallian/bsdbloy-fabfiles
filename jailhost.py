import sys
sys.path.insert(0, './helpers')

import os
import re
import time
import shutil
import yaml

from os import listdir
from os.path import isfile, join
from subprocess import call
from time import gmtime, strftime
from fabric.api import settings
from fabric.api import run
from fabric.api import sudo
from fabric.api import env
from upgrader import Upgrader

import ploy_ezjail

def _get_vars(key, values, listvar):
    if key in values:
        listvar.extend(values[key])

def _get_role_var_files(playbook):
    basedir = playbook.play_basedirs[0]
    roledir = os.path.join(basedir, 'roles')

    var_files = []

    for role in playbook.roles:
        vardir = os.path.join(roledir, role, 'vars')
        var_files.extend([os.path.join(vardir, f) for f in listdir(vardir) if isfile(join(vardir, f))])

    return var_files

def _build_upgrader(ansible_vars, playbook):
    upgrade_services = ['monit']
    upgrade_dirs = []
    upgrade_files = []
    upgrade_postgre_dbs = []
    upgrade_mariadb_dbs = []
    upgrade_sqlite_dbs = []
    upgrade_has_ldap = False

    var_files = _get_role_var_files(playbook)

    for var_file in var_files:
        with open(var_file, 'r') as varfile:
            play_vars = yaml.load(varfile)

            _get_vars('upgrade_services', play_vars, upgrade_services)
            _get_vars('upgrade_dirs', play_vars, upgrade_dirs)
            _get_vars('upgrade_files', play_vars, upgrade_files)
            _get_vars('upgrade_postgre_dbs', play_vars, upgrade_postgre_dbs)
            _get_vars('upgrade_mariadb_dbs', play_vars, upgrade_mariadb_dbs)
            _get_vars('upgrade_sqlite_dbs', play_vars, upgrade_sqlite_dbs)

            if 'upgrade_has_ldap' in play_vars:
                upgrade_has_ldap = play_vars['upgrade_has_ldap']

    _get_vars('upgrade_services', ansible_vars, upgrade_services)
    _get_vars('upgrade_dirs', ansible_vars, upgrade_dirs)
    _get_vars('upgrade_files', ansible_vars, upgrade_files)
    _get_vars('upgrade_postgre_dbs', ansible_vars, upgrade_postgre_dbs)
    _get_vars('upgrade_mariadb_dbs', play_vars, upgrade_mariadb_dbs)
    _get_vars('upgrade_sqlite_dbs', ansible_vars, upgrade_sqlite_dbs)

    upgrader = Upgrader(
        upgrade_services,
        upgrade_dirs,
        upgrade_files,
        upgrade_postgre_dbs,
        upgrade_mariadb_dbs,
        upgrade_sqlite_dbs,
        upgrade_has_ldap,
        ansible_vars)

    return upgrader

def bootstrap():
    bootstrap_conf = """
---
- hosts: jailhost
  user: "{{ jailhost_admin_user }}"
  roles:
    - dhcp_host
    - jails_host
"""

    original_bootstrap_conf = bootstrap_conf
    original_ploy_conf = []

    try:
        with open('../jailhost.yml', 'r') as f:
            original_bootstrap_conf = ''.join(f.readlines())

        with open('../jailhost.yml', 'w') as f:
            f.write(bootstrap_conf)

        original_ploy_conf = []
        tmp_ploy_conf = []
        update_ploy_conf = []
        with open('../etc/ploy.conf', 'r') as f:
            original_ploy_conf = f.readlines()

            for ploy_conf_line in original_ploy_conf:
                if not ploy_conf_line.startswith('fabfile') and \
                        not ploy_conf_line.startswith('user') and \
                        not ploy_conf_line.startswith('sudo'):
                    tmp_ploy_conf.append(ploy_conf_line)

                if  not ploy_conf_line.startswith('user') and \
                        not ploy_conf_line.startswith('sudo'):
                    update_ploy_conf.append(ploy_conf_line)

        with open('../etc/ploy.conf', 'w') as f:
            f.write(''.join(tmp_ploy_conf))

        call(['ploy', '-c', '../etc/ploy.conf', 'bootstrap'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})
        raw_input('Press any key as soon as the system is rebooted...')

        call(['ploy', '-c', '../etc/ploy.conf', 'configure', 'jailhost'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})
        call(['ploy', '-c', '../etc/ploy.conf', 'ssh', 'jailhost', 'pkg install sudo'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})

        with open('../etc/ploy.conf', 'w') as f:
            f.write(''.join(update_ploy_conf))

        call(['ploy', '-c', '../etc/ploy.conf', 'do', 'jailhost', 'update_ports'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})

        with open('../jailhost.yml', 'w') as f:
            f.write(original_bootstrap_conf)

        call(['ploy', '-c', '../etc/ploy.conf', 'configure', 'jailhost'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})

        with open('../etc/ploy.conf', 'w') as f:
            f.write(''.join(original_ploy_conf))
    finally:
        with open('../jailhost.yml', 'w') as f:
            f.write(original_bootstrap_conf)

        with open('../etc/ploy.conf', 'w') as f:
            f.write(''.join(original_ploy_conf))


def update_ports():
    sudo('portsnap fetch')
    sudo('portsnap -p /usr/jails/basejail/usr/ports extract')
    sudo('ln -s /usr/jails/basejail/usr/ports /usr/ports')

def update_ezjail_ports():
    sudo('ezjail-admin update -P')

def terminate(instance):
    zfspath = 'tank/jails/' + instance
    jailpath = os.path.join('/usr/jails', instance)

    sudo('ezjail-admin delete -f ' + instance)
    sudo('umount -f ' + jailpath)
    sudo('rm -rf ' + jailpath)
    sudo ('zfs destroy ' + zfspath)

def upgrade(jailname, ip=None):
    ansible_vars = env.instances[jailname].get_ansible_variables()
    ip = ansible_vars['ploy_ip'] if ip is None else ip
    playbook = env.instances[jailname].get_playbook()

    upgrader = _build_upgrader(ansible_vars, playbook)
    upgrader.upgrade(ip)

def build_backup_paths():
    backup_paths_combined = []
    for instance_name  in env.instances:
        if isinstance(env.instances[instance_name], ploy_ezjail.Instance):
            instance = env.instances[instance_name]
            ansible_vars = instance.get_ansible_variables()
            playbook = instance.get_playbook()

            var_files = _get_role_var_files(playbook)

            backup_paths = []

            for var_file in var_files:
                with open(var_file, 'r') as varfile:
                    play_vars = yaml.load(varfile)

                    _get_vars('backup_dirs', play_vars, backup_paths)

            _get_vars('backup_dirs', ansible_vars, backup_paths)

            jailname = ''.join(instance.uid.split('-')[1:])

            backup_paths_combined.extend(['/usr/jails/{0}{1}'.format(jailname, b) for b in backup_paths])

    backup_paths_combined = list(set(backup_paths_combined))

    jailhost_var_file = '../host_vars/jailhost.yml'
    with open(jailhost_var_file, 'r+') as varfile:
        jailhost_vars = yaml.load(varfile)
        jailhost_vars['backup_paths'] = backup_paths_combined

        varfile.seek(0)
        varfile.write(yaml.dump(jailhost_vars, default_flow_style=False, width=1000))
        varfile.truncate()

    call(['ploy', '-c', '../etc/ploy.conf', 'configure', 'jailhost', '-t', 'backup'], env={'ANSIBLE_HASH_BEHAVIOUR': 'merge', 'PATH': os.environ['PATH'], 'TERM': os.environ['TERM']})

def stop_services(jailname, upgrade_services, warn_only=False):
    with settings(warn_only=warn_only):
        for service in upgrade_services:
            sudo('jexec -U root ' + jailname + ' service ' + service + ' stop')

def start_services(jailname, upgrade_services, warn_only=False):
    with settings(warn_only=warn_only):
        for service in upgrade_services:
            sudo('jexec -U root ' + jailname + ' service ' + service + ' start')

def restore_paths(jailname_old, jailname_new, paths, mkdirs=False):
    with settings(warn_only=True):
        for copy_path in paths:
            backup_path_part = copy_path['path'][1:] \
                if copy_path['path'].startswith('/') else copy_path['path']

            backup_path = os.path.join(
                '/usr/jails/',
                jailname_old,
                backup_path_part)

            restore_path = os.path.join(
                '/usr/jails/',
                jailname_new,
                backup_path_part)

            if mkdirs:
                mkdir_cmd = 'mkdir -p {0}'.format(restore_path)
                sudo(mkdir_cmd)

            copy_cmd = 'rsync -avr {0} {1}'.format(backup_path, restore_path)
            sudo(copy_cmd)

            own_cmd = 'jexec -U root {0} chown -R {1}:{2} {3}'.format(
                jailname_new,
                copy_path['owner'],
                copy_path['group'],
                copy_path['path'])
            sudo(own_cmd)

def backup(jailname, postgre_dbs, mariadb_dbs, has_ldap):
    id = strftime("%Y%m%d%H%M%S", gmtime())

    for backup_db in postgre_dbs:
        backup_cmd_str = 'sudo jexec -U pgsql {0} pg_dump --clean {1} | sudo tee {2} > /dev/null'

        backup_cmd = backup_cmd_str.format(
            jailname,
            backup_db,
            os.path.join('/tmp', id + '_pg_' + backup_db + '.sql'))

        run(backup_cmd, shell_escape=False)
    
    for backup_db in mariadb_dbs:
        backup_cmd_str = 'sudo jexec -U mysql {0} mysqldump {1} | sudo tee {2} > /dev/null'

        backup_cmd = backup_cmd_str.format(
            jailname,
            backup_db,
            os.path.join('/tmp', id + '_maria_' + backup_db + '.sql'))

        run(backup_cmd, shell_escape=False)

    if has_ldap:
        backup_cmd_str = 'sudo jexec -U root {0} slapcat -l {1}'

        backup_cmd = backup_cmd_str.format(
            jailname,
            os.path.join('/tmp', id + '.ldif'))

        run(backup_cmd, shell_escape=False)

    return id

def restore(
        jailname_old,
        jailname_new,
        postgre_dbs,
        mariadb_dbs,
        sqlite_dbs,
        has_ldap,
        dirs,
        files,
        mounts,
        id):
    for backup_db in postgre_dbs:
        jail_db_file = os.path.join(
            '/usr/jails',
            jailname_new,
            'tmp',
            'pg_' + backup_db + '.sql')

        db_backup_file = os.path.join('/tmp', id + '_pg_' + backup_db + '.sql')

        sql_copy_cmd = 'cp {0} {1}'.format(
            db_backup_file,
            jail_db_file)
        sudo(sql_copy_cmd)

        restore_cmd = 'jexec -U pgsql {0} psql -d {1} -f {2}'.format(
            jailname_new,
            backup_db,
            os.path.join('/tmp', 'pg_' + backup_db + '.sql'))
        sudo(restore_cmd, shell_escape=False)

        rm_cmd = 'rm {0}'.format(jail_db_file)
        sudo(rm_cmd)

    for backup_db in mariadb_dbs:
        jail_db_file = os.path.join(
            '/usr/jails',
            jailname_new,
            'tmp',
            'maria_' + backup_db + '.sql')

        db_backup_file = os.path.join('/tmp', id + '_maria_' + backup_db + '.sql')

        sql_copy_cmd = 'cp {0} {1}'.format(
            db_backup_file,
            jail_db_file)
        sudo(sql_copy_cmd)
        
        restore_cmd = "jexec -U root {0} sh -c 'mysql --database={1} < {2}'".format(
            jailname_new,
            backup_db,
            os.path.join('/tmp', 'maria_' + backup_db + '.sql'))
        sudo(restore_cmd, shell_escape=False)

        rm_cmd = 'rm {0}'.format(jail_db_file)
        sudo(rm_cmd)

    if has_ldap:
        jail_ldif_file = os.path.join(
            '/usr/jails',
            jailname_new,
            'tmp',
            id + '.ldif')

        oldjail_ldif_file = os.path.join('/usr/jails', jailname_old, 'tmp', id + '.ldif')
        ldif_backup_file = os.path.join('/tmp', id + '.ldif')

        ldif_copy_cmd = 'cp {0} {1}'.format(
            oldjail_ldif_file,
            jail_ldif_file)
        sudo(ldif_copy_cmd)

        drop_cmd = 'jexec -U root {0} rm /var/db/openldap-data/data.mdb'.format(
            jailname_new)
        sudo(drop_cmd)

        restore_cmd = 'jexec -U root {0} slapadd -l {1}'.format(
            jailname_new,
            ldif_backup_file,
            os.path.join('/tmp', id + '.ldif'))
        sudo(restore_cmd, shell_escape=False)

        rm_cmd = 'rm {0}'.format(jail_ldif_file)
        sudo(rm_cmd)

        slapd_restart_cmd = 'jexec -U root {0} service slapd restart'.format(
            jailname_new)
        sudo(slapd_restart_cmd)

    restore_paths(jailname_old, jailname_new, dirs, True)
    restore_paths(jailname_old, jailname_new, files)

    for backup_db in sqlite_dbs:
        backup_path = os.path.join(
            '/usr/jails/',
            jailname_old,
            backup_db['path'][1:])

        db_restore_path = os.path.join(
            '/usr/jails',
            jailname_new,
            backup_db['path'][1:])

        backup_cmd = 'sqlite3 {0} ".backup {1}"'.format(
            backup_path,
            db_restore_path)
        sudo(backup_cmd, shell_escape=False, shell=False)

        own_cmd = 'jexec -U root {0} chown -R {1}:{2} {3}'.format(
            jailname_new,
            backup_db['owner'],
            backup_db['group'],
            backup_db['path'])
        sudo(own_cmd)
