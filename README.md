# BSDploy Fabfiles
[Fabfiles](http://www.fabfile.org/) to be used together with [BSDploy](https://github.com/ployground/bsdploy) and my FreeBSD roles.

The most important part is the ability to upgrade jails. In order for this to work the jail must reference the *fab.py* fabfile and it may define some variables (none of which are required):

### ansible-services
A list of services the jail uses. These get stopped during the upgrade. Do not put postgresql into the list otherwise the database backup will fail.

### ansible-backup_postgre_dbs
A list of PostgreSQL database names to be migrated.

### ansible-backup_sqlite_dbs
A list of sqlite database paths to be migrated.

### ansible-backup_dirs
A list of directories to be migrated with the following syntax:

```
{directory_path}:{user}:{group}
```

### ansible-backup_files
A list of single files to be migrated. Use the same syntax as for the directories.


To run an upgrade do the following:
```
ploy do {name_of_your_jailhost} upgrade {name_of_the_jail} <new_ip>
```

The ip is optional, by default it uses the same as the old jail.

Depending on the amount of data to be moved around this can take a while (eg when you are upgrading an owncloud jail with lots of files).

If it ran through without problems test everything you need to test and if something is wrong you just start the old jail again and figure out what is wrong with the new one.

On the other hand if all is well, you can terminate the old jail like this:
```
ploy do {name_of_your_jailhost} terminate {name_of_the_old_jail}
```
